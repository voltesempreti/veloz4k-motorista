package com.veloz4k.driver.ui.fragment.upcoming;


import com.veloz4k.driver.base.MvpPresenter;

public interface UpcomingTripIPresenter<V extends UpcomingTripIView> extends MvpPresenter<V> {

    void getUpcoming();

}
