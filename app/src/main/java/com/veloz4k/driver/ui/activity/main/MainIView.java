package com.veloz4k.driver.ui.activity.main;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.EstimateFare;
import com.veloz4k.driver.data.network.model.SettingsResponse;
import com.veloz4k.driver.data.network.model.TripResponse;
import com.veloz4k.driver.data.network.model.UserResponse;

public interface MainIView extends MvpView {
    void onSuccess(UserResponse user);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

    void onSuccess(TripResponse tripResponse);

    void onSuccess(SettingsResponse response);

    void onSettingError(Throwable e);

    void onSuccessProviderAvailable(Object object);

    void onSuccessFCM(Object object);

    void onSuccessLocationUpdate(TripResponse tripResponse);

    void onSuccessInstantNow(Object object);

    void onSuccessInstant(EstimateFare object);

    void onSuccess(EstimateFare estimateFare);

}
