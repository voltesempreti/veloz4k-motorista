package com.veloz4k.driver.ui.activity.reset_password;

import com.veloz4k.driver.base.MvpView;

public interface ResetIView extends MvpView{

    void onSuccess(Object object);
    void onError(Throwable e);
}
