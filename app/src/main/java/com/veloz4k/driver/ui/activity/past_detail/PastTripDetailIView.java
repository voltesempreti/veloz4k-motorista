package com.veloz4k.driver.ui.activity.past_detail;


import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.HistoryDetail;

public interface PastTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
