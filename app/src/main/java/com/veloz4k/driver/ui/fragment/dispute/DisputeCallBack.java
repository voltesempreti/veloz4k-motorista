package com.veloz4k.driver.ui.fragment.dispute;

public interface DisputeCallBack {
    void onDisputeCreated();
}
