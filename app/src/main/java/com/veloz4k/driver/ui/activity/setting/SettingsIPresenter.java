package com.veloz4k.driver.ui.activity.setting;

import com.veloz4k.driver.base.MvpPresenter;

public interface SettingsIPresenter<V extends SettingsIView> extends MvpPresenter<V> {
    void changeLanguage(String languageID);
}
