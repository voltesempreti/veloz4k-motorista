package com.veloz4k.driver.ui.activity.notification_manager;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.NotificationManager;

import java.util.List;

public interface NotificationManagerIView extends MvpView {

    void onSuccess(List<NotificationManager> managers);

    void onError(Throwable e);

}