package com.veloz4k.driver.ui.activity.request_money;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.RequestDataResponse;

public interface RequestMoneyIView extends MvpView {

    void onSuccess(RequestDataResponse response);
    void onSuccess(Object response);
    void onError(Throwable e);

}
