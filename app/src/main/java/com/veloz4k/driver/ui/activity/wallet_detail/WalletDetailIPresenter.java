package com.veloz4k.driver.ui.activity.wallet_detail;

import com.veloz4k.driver.base.MvpPresenter;
import com.veloz4k.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIPresenter<V extends WalletDetailIView> extends MvpPresenter<V> {
    void setAdapter(ArrayList<Transaction> myList);
}
