package com.veloz4k.driver.ui.activity.summary;


import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.Summary;

public interface SummaryIView extends MvpView {

    void onSuccess(Summary object);

    void onError(Throwable e);
}
