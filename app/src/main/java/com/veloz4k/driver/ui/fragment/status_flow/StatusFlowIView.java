package com.veloz4k.driver.ui.fragment.status_flow;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.TimerResponse;

public interface StatusFlowIView extends MvpView {

    void onSuccess(Object object);

    void onWaitingTimeSuccess(TimerResponse object);

    void onError(Throwable e);
}
