package com.veloz4k.driver.ui.activity.earnings;


import com.veloz4k.driver.base.MvpPresenter;

public interface EarningsIPresenter<V extends EarningsIView> extends MvpPresenter<V> {

    void getEarnings();
}
