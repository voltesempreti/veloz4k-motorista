package com.veloz4k.driver.ui.activity.invite_friend;

import com.veloz4k.driver.base.MvpPresenter;

public interface InviteFriendIPresenter<V extends InviteFriendIView> extends MvpPresenter<V> {
    void profile();
}
