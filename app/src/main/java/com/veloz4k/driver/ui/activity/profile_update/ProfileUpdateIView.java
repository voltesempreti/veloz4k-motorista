package com.veloz4k.driver.ui.activity.profile_update;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.UserResponse;

public interface ProfileUpdateIView extends MvpView {

    void onSuccess(UserResponse user);

    void onSuccessUpdate(UserResponse object);

    void onError(Throwable e);

    void onSuccessPhoneNumber(Object object);

    void onVerifyPhoneNumberError(Throwable e);

}
