package com.veloz4k.driver.ui.activity.splash;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.CheckVersion;

public interface SplashIView extends MvpView {

    void verifyAppInstalled();

    void onSuccess(Object user);

    void onSuccess(CheckVersion user);

    void onError(Throwable e);

    void onCheckVersionError(Throwable e);
}
