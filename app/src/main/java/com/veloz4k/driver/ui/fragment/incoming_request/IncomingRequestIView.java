package com.veloz4k.driver.ui.fragment.incoming_request;

import com.veloz4k.driver.base.MvpView;

public interface IncomingRequestIView extends MvpView {

    void onSuccessAccept(Object responseBody);
    void onSuccessCancel(Object object);
    void onError(Throwable e);
}
