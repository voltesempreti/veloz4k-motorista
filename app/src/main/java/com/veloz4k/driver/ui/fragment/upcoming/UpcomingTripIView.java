package com.veloz4k.driver.ui.fragment.upcoming;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.HistoryList;

import java.util.List;

public interface UpcomingTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
