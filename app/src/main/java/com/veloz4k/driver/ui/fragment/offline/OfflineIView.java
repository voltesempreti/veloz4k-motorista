package com.veloz4k.driver.ui.fragment.offline;

import com.veloz4k.driver.base.MvpView;

public interface OfflineIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
