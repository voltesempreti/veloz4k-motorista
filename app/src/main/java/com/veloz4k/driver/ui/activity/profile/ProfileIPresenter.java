package com.veloz4k.driver.ui.activity.profile;

import com.veloz4k.driver.base.MvpPresenter;

public interface ProfileIPresenter<V extends ProfileIView> extends MvpPresenter<V> {

    void getProfile();

}
