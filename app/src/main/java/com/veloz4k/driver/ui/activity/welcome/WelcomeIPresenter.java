package com.veloz4k.driver.ui.activity.welcome;

import com.veloz4k.driver.base.MvpPresenter;

public interface WelcomeIPresenter<V extends WelcomeIView> extends MvpPresenter<V> {
}
