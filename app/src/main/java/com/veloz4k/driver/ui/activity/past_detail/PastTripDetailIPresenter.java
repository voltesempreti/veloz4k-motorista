package com.veloz4k.driver.ui.activity.past_detail;


import com.veloz4k.driver.base.MvpPresenter;

public interface PastTripDetailIPresenter<V extends PastTripDetailIView> extends MvpPresenter<V> {

    void getPastTripDetail(String request_id);
}
