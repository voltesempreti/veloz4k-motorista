package com.veloz4k.driver.ui.fragment.past;


import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.HistoryList;

import java.util.List;

public interface PastTripIView extends MvpView {

    void onSuccess(List<HistoryList> historyList);
    void onError(Throwable e);
}
