package com.veloz4k.driver.ui.activity.email;

import com.veloz4k.driver.base.MvpPresenter;

public interface EmailIPresenter<V extends EmailIView> extends MvpPresenter<V> {
}
