package com.veloz4k.driver.ui.bottomsheetdialog.invoice_flow;

import com.veloz4k.driver.base.MvpView;

public interface InvoiceDialogIView extends MvpView {

    void onSuccess(Object object);
    void onError(Throwable e);
}
