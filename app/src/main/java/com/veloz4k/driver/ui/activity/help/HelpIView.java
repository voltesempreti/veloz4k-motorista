package com.veloz4k.driver.ui.activity.help;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.Help;

public interface HelpIView extends MvpView {

    void onSuccess(Help object);

    void onError(Throwable e);
}
