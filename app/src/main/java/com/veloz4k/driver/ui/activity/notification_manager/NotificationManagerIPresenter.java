package com.veloz4k.driver.ui.activity.notification_manager;

import com.veloz4k.driver.base.MvpPresenter;

public interface NotificationManagerIPresenter<V extends NotificationManagerIView> extends MvpPresenter<V> {
    void getNotificationManager();
}
