package com.veloz4k.driver.ui.activity.forgot_password;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.ForgotResponse;

public interface ForgotIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);
    void onError(Throwable e);
}
