package com.veloz4k.driver.ui.activity.location_pick;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.AddressResponse;

/**
 * Created by santhosh@appoets.com on 19-05-2018.
 */
public interface LocationPickIView extends MvpView {

    void onSuccess(AddressResponse address);
    void onError(Throwable e);
}
