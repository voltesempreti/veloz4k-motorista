package com.veloz4k.driver.ui.activity.instant_ride;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.EstimateFare;
import com.veloz4k.driver.data.network.model.TripResponse;

public interface InstantRideIView extends MvpView {

    void onSuccess(EstimateFare estimateFare);

    void onSuccess(TripResponse response);

    void onError(Throwable e);

}
