package com.veloz4k.driver.ui.activity.earnings;


import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.EarningsList;

public interface EarningsIView extends MvpView {

    void onSuccess(EarningsList earningsLists);

    void onError(Throwable e);
}
