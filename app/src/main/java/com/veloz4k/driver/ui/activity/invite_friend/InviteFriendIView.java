package com.veloz4k.driver.ui.activity.invite_friend;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.UserResponse;

public interface InviteFriendIView extends MvpView {

    void onSuccess(UserResponse response);
    void onError(Throwable e);

}
