package com.veloz4k.driver.ui.activity.help;


import com.veloz4k.driver.base.MvpPresenter;

public interface HelpIPresenter<V extends HelpIView> extends MvpPresenter<V> {

    void getHelp();
}
