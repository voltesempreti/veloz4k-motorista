package com.veloz4k.driver.ui.activity.setting;

import com.veloz4k.driver.base.MvpView;

public interface SettingsIView extends MvpView {

    void onSuccess(Object o);

    void onError(Throwable e);

}
