package com.veloz4k.driver.ui.activity.wallet;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.WalletMoneyAddedResponse;
import com.veloz4k.driver.data.network.model.WalletResponse;

public interface WalletIView extends MvpView {

    void onSuccess(WalletResponse response);

    void onSuccess(WalletMoneyAddedResponse response);

    void onError(Throwable e);
}
