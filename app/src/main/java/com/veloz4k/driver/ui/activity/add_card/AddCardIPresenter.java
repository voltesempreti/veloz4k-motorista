package com.veloz4k.driver.ui.activity.add_card;

import com.veloz4k.driver.base.MvpPresenter;

public interface AddCardIPresenter<V extends AddCardIView> extends MvpPresenter<V> {

    void addCard(String stripeToken);
}
