package com.veloz4k.driver.ui.activity.summary;


import com.veloz4k.driver.base.MvpPresenter;

public interface SummaryIPresenter<V extends SummaryIView> extends MvpPresenter<V> {

    void getSummary(String data);
}
