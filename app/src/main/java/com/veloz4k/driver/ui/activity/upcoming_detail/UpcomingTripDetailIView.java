package com.veloz4k.driver.ui.activity.upcoming_detail;


import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.HistoryDetail;

public interface UpcomingTripDetailIView extends MvpView {

    void onSuccess(HistoryDetail historyDetail);
    void onError(Throwable e);
}
