package com.veloz4k.driver.ui.fragment.dispute;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.DisputeResponse;

import java.util.List;

public interface DisputeIView extends MvpView {

    void onSuccessDispute(List<DisputeResponse> responseList);

    void onSuccess(Object object);

    void onError(Throwable e);
}
