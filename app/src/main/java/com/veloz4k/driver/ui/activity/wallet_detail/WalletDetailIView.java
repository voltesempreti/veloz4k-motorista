package com.veloz4k.driver.ui.activity.wallet_detail;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.Transaction;

import java.util.ArrayList;

public interface WalletDetailIView extends MvpView {
    void setAdapter(ArrayList<Transaction> myList);
}
