package com.veloz4k.driver.ui.activity.profile;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.UserResponse;

public interface ProfileIView extends MvpView {

    void onSuccess(UserResponse user);

    void onError(Throwable e);

}
