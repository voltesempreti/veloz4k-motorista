package com.veloz4k.driver.ui.activity.your_trips;

import com.veloz4k.driver.base.MvpPresenter;

public interface YourTripIPresenter<V extends YourTripIView> extends MvpPresenter<V> {
}
