package com.veloz4k.driver.ui.activity.earnings;

import com.veloz4k.driver.base.BasePresenter;
import com.veloz4k.driver.data.network.APIClient;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class EarningsPresenter<V extends EarningsIView> extends BasePresenter<V> implements EarningsIPresenter<V> {
    @Override
    public void getEarnings() {
        getCompositeDisposable().add(
                APIClient
                        .getAPIClient()
                        .getEarnings()
                        .subscribeOn(Schedulers.computation())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(
                                trendsResponse -> getMvpView().onSuccess(trendsResponse),
                                throwable -> getMvpView().onError(throwable)
                        )
        );
    }
}
