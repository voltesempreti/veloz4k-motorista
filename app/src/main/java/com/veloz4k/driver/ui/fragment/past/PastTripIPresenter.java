package com.veloz4k.driver.ui.fragment.past;


import com.veloz4k.driver.base.MvpPresenter;

public interface PastTripIPresenter<V extends PastTripIView> extends MvpPresenter<V> {

    void getHistory();

}
