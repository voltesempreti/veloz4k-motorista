package com.veloz4k.driver.ui.activity.sociallogin;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.Token;

public interface SocialLoginIView extends MvpView {

    void onSuccess(Token token);
    void onError(Throwable e);
}
