package com.veloz4k.driver.ui.activity.password;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.ForgotResponse;
import com.veloz4k.driver.data.network.model.User;

public interface PasswordIView extends MvpView {

    void onSuccess(ForgotResponse forgotResponse);

    void onSuccess(User object);

    void onError(Throwable e);
}
