package com.veloz4k.driver.ui.activity.add_card;

import com.veloz4k.driver.base.MvpView;

public interface AddCardIView extends MvpView {

    void onSuccess(Object card);

    void onError(Throwable e);
}
