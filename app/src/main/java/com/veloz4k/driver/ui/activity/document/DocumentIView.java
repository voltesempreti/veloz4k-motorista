package com.veloz4k.driver.ui.activity.document;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.DriverDocumentResponse;

public interface DocumentIView extends MvpView {

    void onSuccess(DriverDocumentResponse response);

    void onDocumentSuccess(DriverDocumentResponse response);

    void onError(Throwable e);

    void onSuccessLogout(Object object);

}
