package com.veloz4k.driver.ui.activity.change_password;

import com.veloz4k.driver.base.MvpView;

public interface ChangePasswordIView extends MvpView {


    void onSuccess(Object object);
    void onError(Throwable e);
}
