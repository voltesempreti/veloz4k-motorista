package com.veloz4k.driver.ui.bottomsheetdialog.rating;

import com.veloz4k.driver.base.MvpView;
import com.veloz4k.driver.data.network.model.Rating;

public interface RatingDialogIView extends MvpView {

    void onSuccess(Rating rating);
    void onError(Throwable e);
}
