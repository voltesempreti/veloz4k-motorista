package com.veloz4k.driver.common.swipe_button;

public interface OnActiveListener {
    void onActive();
}
