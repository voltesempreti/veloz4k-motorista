package com.veloz4k.driver.common.swipe_button;

public interface OnStateChangeListener {
    void onStateChange(boolean active);
}
